import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

class Pattern(object):
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}
    
    def teardown_method(self, method):
        self.driver.quit()

    def login_to_user(self):
        self.driver.get("https://trello.com/")
        self.driver.set_window_size(1280, 696)
        time.sleep(15)
        self.driver.find_element(By.TAG_NAME, "a[href='/login']").click()
        self.driver.find_element(By.ID, "user").click()
        self.driver.find_element(By.ID, "user").send_keys("enrique.rivera@fundacion-jala.org")
        self.driver.find_element(By.ID, "login").click()
        time.sleep(5)
        self.driver.find_element(By.ID, "password").click()
        self.driver.find_element(By.ID, "password").send_keys("trelloapi")
        self.driver.find_element(By.ID, "login-submit").click()
        time.sleep(30)

    def create_a_board(self):
        self.driver.find_element(By.CSS_SELECTOR, "div.board-tile,mod-add").click()
        self.driver.find_element(By.TAG_NAME, "input[data-test-id=create-board-title-input]").send_keys("Test")
        self.driver.find_element(By.TAG_NAME, "input[data-test-id=create-board-title-input]").send_keys(Keys.ENTER)
        time.sleep(3)

    def delete_a_board(self):
        time.sleep(5)
        self.driver.find_element(By.CSS_SELECTOR, "a.js-show-sidebar").click()
        self.driver.find_element(By.CSS_SELECTOR, "a.js-open-more").click()
        time.sleep(2)
        self.driver.find_element(By.CSS_SELECTOR, "a.js-close-board").click()
        element = self.driver.find_element(By.CSS_SELECTOR, "a.js-close-board")
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()
        element = self.driver.find_element(By.CSS_SELECTOR, "body")
        actions = ActionChains(self.driver)
        time.sleep(3)
        self.driver.find_element(By.CSS_SELECTOR, "input.js-confirm").click()
        self.driver.find_element(By.CSS_SELECTOR, "a.quiet").click()
        element = self.driver.find_element(By.CSS_SELECTOR, "a.quiet")
        actions = ActionChains(self.driver)
        actions.move_to_element(element).perform()
        element = self.driver.find_element(By.CSS_SELECTOR, "body")
        actions = ActionChains(self.driver)
        self.driver.find_element(By.CSS_SELECTOR, "input.js-confirm").click()